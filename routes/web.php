<?php
use \Illuminate\Support\Facades\Redirect;
use  \Illuminate\Support\Facades;
Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get("logout", function (){
    Auth::logout();
    return Redirect(route("home"));
});


Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin', 'middleware'=>'admin'], function(){
    $prefix = 'Admin';
    Route::get('/', $prefix.'\DashboardController@index')->name('admin_dashboard');
    //Content
    Route::get('/contents','Admin\ContentController@index')->name('admin_content_index');
    Route::get('/content/{id}','Admin\ContentController@detail')->name('admin_content_detail');
    Route::get('/contents/get','Admin\ContentController@get')->name('admin_content_get');
    Route::post('/content/create','Admin\ContentController@create')->name('admin_content_create');
    Route::post('/content/update','Admin\ContentController@update')->name('admin_content_update');
    Route::post('/content/delete','Admin\ContentController@delete')->name('admin_content_delete');
    //Chapters
    Route::get('/chapters','Admin\ChapterController@index')->name('admin_chapter_index');
    Route::post('/chapter/create','Admin\ChapterController@create')->name('admin_chapter_create');
    Route::post('/chapter/delete','Admin\ChapterController@delete')->name('admin_chapter_delete');
    Route::get('/chapter/get','Admin\ChapterController@get')->name('admin_chapter_get');
    Route::post('/chapter/update','Admin\ChapterController@update')->name('admin_chapter_update');
    //Lessons
    Route::get('/lessons','Admin\LessonController@index')->name('admin_lesson_index');
    Route::post('/lesson/delete','Admin\LessonController@delete')->name('admin_lesson_delete');
    Route::post('/lesson/create','Admin\LessonController@create')->name('admin_lesson_create');
    Route::get('/lesson/get','Admin\LessonController@get')->name('admin_lesson_get');
    Route::post('/lesson/update','Admin\LessonController@update')->name('admin_lesson_update');
    //sentences
    Route::get('/sentences','Admin\SentenceController@index')->name('admin_sentence_index');
    Route::post('/sentence/create','Admin\SentenceController@create')->name('admin_sentence_create');
    Route::get('/sentence/get','Admin\SentenceController@get')->name('admin_sentence_get');
    Route::post('/sentence/update','Admin\SentenceController@update')->name('admin_sentence_update');
    //word
    Route::get('/words','Admin\WordController@index')->name('admin_word_index');
    Route::post('/word/create','Admin\WordController@create')->name('admin_word_create');
    //question
    Route::get('/questions','Admin\QuestionController@index')->name('admin_question_index');
    Route::post('/questions/create','Admin\QuestionController@create')->name('admin_question_create');
    //answer
    Route::get('/answers','Admin\AnswerController@index')->name('admin_answer_index');
    Route::post('/answers/create','Admin\AnswerController@create')->name('admin_answer_create');
});

Route::get('api/list','ListAPIController@index');

Route::get('create-zip', 'ZipArchiveController@index')->name('create-zip');
//Route::group(['prefix'=>'teacher', 'middleware'=>'teacher'], function(){
//    $prefix = 'Teacher';
//    Route::get('/', $prefix.'\DashboardController@index')->name('teacher_dashboard');
//});
//
//
//Route::group(['prefix'=>'student', 'middleware'=>'student'], function(){
//    $prefix = 'Student';
//    Route::get('/', $prefix.'\DashboardController@index')->name('student_dashboard');
//});

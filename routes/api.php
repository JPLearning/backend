<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    //API V1
    $prefix = 'Api';
 //   Route::post('/register',$prefix.'\Authentication\Authentication@register')->name('register_api');
    Route::post('/login',$prefix.'\Authentication\Authentication@login')->name('login_api');


    Route::get('/getListContent',$prefix.'\LessonHistoryController@getListContent');
    Route::get('/getContentByid',$prefix.'\LessonHistoryController@getContentByid');
    Route::get('/getChapterByid',$prefix.'\LessonHistoryController@getChapterByid');
    Route::get('/getWordByLessonID',$prefix.'\LessonHistoryController@getWordByLessonID');
    Route::get('/getListLessonByChapterID',$prefix.'\LessonHistoryController@getListLessonByChapterID');
    Route::get('/getListChapter',$prefix.'\LessonHistoryController@getListChapter');
    Route::get('/phraseLearn',$prefix.'\LessonHistoryController@phraseLearn');
    Route::get('/listword',$prefix.'\LessonHistoryController@listword');
    Route::get('/conversation',$prefix.'\LessonHistoryController@conversation');
    // Get list question by lesson
    Route::get('/question',$prefix.'\LessonHistoryController@question');
    // Get list answer by question
    Route::get('/answer',$prefix.'\LessonHistoryController@answer');
    // Get list Q_A
    Route::get('/listQA',$prefix.'\LessonHistoryController@listQA');


    //Route::put('/updateReport/{id}',$prefix.'\ReportController@updateTime');
    //Route::get('/getReport1/{type}/{id}',$prefix.'\ReportController@getReport1');

    Route::post('/lessonUpdate',$prefix.'\LessonHistoryController@lessonUpdate');

    Route::get('/getReport',$prefix.'\ReportController@getReport');

});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartLearningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('part_learnings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->tinyInteger('listening')->default(0)->comment('0: inactive, 1: active');
            $table->tinyInteger('remember')->default(0)->comment('0: inactive, 1: active');
            $table->tinyInteger('speaking')->default(0)->comment('0: inactive, 1: active');
            $table->string('time_start');
            $table->string('time_end');
            $table->string('word');
            $table->integer('lesson_id')->unsigned();
            $table->foreign('lesson_id')
                ->references('id')
                ->on('lessons')
                ->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('part_learnings');
    }
}

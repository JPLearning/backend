<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class UserLessonStudy extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_lesson_study';
    public $timestamps = false;
    protected $fillable = ['lesson_id','type_id','user_id','word','status_id','startDate','endDate'];

}

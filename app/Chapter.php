<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Chapter extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chapters';
    public $timestamps = false;
    protected $fillable = ['name','icon','content_id','number_lesson'];
    public function content(){
        return $this->belongsTo('App\Content','content_id','id');
    }
    public function lesson(){
        return $this->hasMany('App\Lesson','chapter_id','id');
    }
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->updated_at = $model->freshTimestamp();
        });
    }
}

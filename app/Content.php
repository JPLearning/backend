<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Content extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'contents';
    public $timestamps = false;
    protected $fillable = ['name','description','icon','author','filename',	'number_chapter'];
    public function user(){
        return $this->belongsTo('App\User','author','id');
    }
    public function chapter(){
        return $this->hasMany('App\Chapter','content_id','id');
    }
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->updated_at = $model->freshTimestamp();
        });
    }

}

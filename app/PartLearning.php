<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class PartLearning extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'part_learnings';
    public $timestamps = false;
    protected $fillable = ['id', 'name', 'listening', 'remember', 'speaking','time_start','time_end','lesson_id','user_id'];
}

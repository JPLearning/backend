<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Video extends Model
{
    use Notifiable;
    protected $table = 'videos';
    public $timestamps = false;
    protected $fillable = ['name','subtitle','lesson_id'];
    //
    public function wordlearning(){
        return $this->belongsTo('App\WordLearning');
    }
}

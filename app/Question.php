<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Question extends Model
{
    use Notifiable;
    protected $table = 'questions';
    public $timestamps = false;
    protected $fillable = ['question','question_vi','mp3','lesson_id'];
    //
    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }
    public function answer(){
        return $this->hasMany('App\Answer');
    }

}

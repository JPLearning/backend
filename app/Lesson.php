<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Lesson extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lessons';
    public $timestamps = false;
    protected $fillable = ['name','chapter_id','video','sub','word','phrase'];
    public function chapter(){
        return $this->belongsTo('App\Chapter','chapter_id','id');
    }
    public function sentence(){
        return $this->hasMany('App\Sentence');
    }
    public function word(){
        return $this->hasMany('App\Word');
    }
    public function question(){
        return $this->hasMany('App\Question');
    }
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->updated_at = $model->freshTimestamp();
        });
    }
}

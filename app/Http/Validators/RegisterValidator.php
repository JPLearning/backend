<?php
namespace App\Http\Validators;
use Illuminate\Support\Facades\Validator;
class RegisterValidator extends Validator
{
    protected $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:8|confirmed',
        'confirm_password' => 'required|string|min:8|same:password'
    ];
}
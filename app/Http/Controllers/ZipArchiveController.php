<?php

namespace App\Http\Controllers;
use ZipArchive;

use Illuminate\Http\Request;

class ZipArchiveController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('download')) {
            // Define Dir Folder
            $public_dir=public_path('test');
            // Zip File Name
            $zipFileName = 'AllDocuments.zip';
            $rootPath = './';
            // create recursive directory iterator
            $files = glob('folder/*.{txt,mp3,jpg,png,gif}', GLOB_BRACE);
            // Create ZipArchive Obj
            $zip = new ZipArchive;
            if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
                // Add File in ZipArchive
                foreach($files as $file) {
                    $zip->addFile('test','abc.txt');
                    $zip->addFile('test','def.txt');
                }

                // Close ZipArchive
                $zip->close();
            }
            // Set Header
            $headers = array(
                'Content-Type' => 'application/octet-stream',
            );
            $filetopath=$public_dir.'/'.$zipFileName;
            // Create Download Response
            if(file_exists($filetopath)){
                return response()->download($filetopath,$zipFileName,$headers);
            }
        }
        return view('createZip');
    }
}

<?php

namespace App\Http\Controllers\Api\Authentication;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Query\Builder ;

class Authentication extends Controller
{
    public function validateUser(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
            'confirm_password' => 'required|string|min:8|same:password'
        ]);
    }

    public function register(Request $request)
    {
        try {
            $user = User::where('email', $request->email)->first();
            if ($user == null) {
                $user = new User();
                $user->name = $request->name;
                $user->password = Hash::make($request->password);
                $user->email = $request->email;
                $user->role_id = $request->role_id;
                $user->save();
                return response()->json(['status' => 'success', 'data' => $user]);
            } else {
                return response()->json(['status' => 'error', 'message' => $request->password . ' has been registered']);
            }

        } catch (Exception $exception) {
            return response()->json(['status' => 'error', 'data' => $exception]);
        }
    }

    public function login(Request $request)
    {
        $result['error'] = 0;
        $result['data'] = array();
        try {
            $user = User::where('email', $request->email)->first();
            if ($user == null) {
                return json_encode($result);
            } else {
                if (Hash::check($request->password, $user->password))  {
                    $user->generateToken();
                    $result["data"] = json_decode(json_encode($user), True);
                    return json_encode($result);
                }
                return json_encode($result);
            }

        } catch (Exception $exception) {
            return json_encode($result);
        }

    }

}

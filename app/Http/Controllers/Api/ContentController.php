<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ContentController extends Controller
{
    public function show($id)
    {
        $content_detail = DB::table('users')
            ->join('contents', 'contents.user_id', '=', 'users.id')
            ->join('chapters', 'chapters.content_id', '=', 'contents.id')
            ->join('lessons', 'lessons.chapter_id', '=', 'chapters.id')
            ->join('videos', 'videos.lesson_id', '=', 'lessons.id')
            ->join('word_learnings', 'word_learnings.video_id', '=', 'videos.id')

            ->select('users.id','contents.id','contents.name',
                'chapters.id','chapters.name','lessons.id','lessons.name',
                'videos.name','videos.subtitle','word_learnings.word')
            ->where('users.id', '=', $id)
            ->get();
        return  $content_detail;
    }

}
<?php

namespace App\Http\Controllers\Api;

use App\PartLearning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ReportController extends Controller
{

    public function getReport1($type,$id)
    {
        $data = array();
        $data1 = array();
        switch ($type) {
            case 1://report by lesson
            {
                $query = DB::table("users")->where('id', $id)->get();
                //  print_r($query);
                $Lessons = DB::table("lessons")->where('user_id', $query[0]->id)->get();

                for ($i = 0; $i < count($Lessons); $i++) {
                    $data["id"] = $Lessons[$i]->id;
                    $data["name"] = $Lessons[$i]->name;

                    $part_learning = DB::table("part_learnings")->where('lesson_id', $Lessons[$i]->id)
                        ->select("listening", "remember", "speaking", "time_start", "time_end")->get();
                    $comments = DB::table("comments")->where('lesson_id', $Lessons[$i]->id)->select("content")->get();
                    $array_part_learning = json_decode(json_encode($part_learning), True);
                    // $array_coments = json_decode(json_encode($comments), True);
                    $data["part_learnings"] = $array_part_learning;
                    $data["comments"] = $comments;
                    $data1[$i] = $data;

                }
                $result = array();
                $result["error"] = 0;
                $result["data"] = $data1;
                return json_encode($result);
                break;
            }
            case 2: //report by word
            {
                $query = DB::table("users")->where('id', $id)->get();
                //  print_r($query);
                $Lessons = DB::table("lessons")->where('user_id', $query[0]->id)->get();
                $Words = DB::table("words")->where('lesson_id', $Lessons[0]->id)->get();

                for ($i = 0; $i < count($Words); $i++) {
                    $data["id"] = $Words[$i]->id;
                    $data["word"] = $Words[$i]->word;
                    $data["status"] = $Words[$i]->status;
                    $data["level"] = $Words[$i]->level;
                    // get list of chapter

                    $comments = DB::table("comments")->where('word_id', $Words[$i]->id)->select("content")->get();
                    $data["comments"] = $comments;
                    $data1[$i] = $data;

                }
                $result = array();
                $result["error"] = 0;
                $result["data"] = $data1;
                return json_encode($result);
                break;
            }
            case 3: //report by sentence
            {
                $query = DB::table("users")->where('id', $id)->get();
                //  print_r($query);
                $Lessons = DB::table("lessons")->where('user_id', $query[0]->id)->get();
                $Sentences = DB::table("sentences")->where('lesson_id', $Lessons[0]->id)->get();

                for ($i = 0; $i < count($Sentences); $i++) {
                    $data["id"] = $Sentences[$i]->id;
                    $data["sentence"] = $Sentences[$i]->sentence;
                    $data["status"] = $Sentences[$i]->status;
                    $data["level"] = $Sentences[$i]->level;

                    $comments = DB::table("comments")->where('word_id', $Sentences[$i]->id)->select("content")->get();
                    $data["comments"] = $comments;
                    $data1[$i] = $data;

                }
                $result = array();
                $result["error"] = 0;
                $result["data"] = $data1;
                return json_encode($result);
                break;
            }
        }
    }
      // return json_encode($result);

        // $Lessons = DB::table("lessons")->get();

    public function updateTime(Request $request,$id)
    {
        $result['error'] = 0;
        $result['data'] = array();
        $part_learning = PartLearning::where('id', '=', $id) -> first();
        //$part_learning = DB::table("part_learnings")->where('id',$id)->get();;
        $result["data"] = json_decode(json_encode($part_learning), True);
      // print_r($part_learning);
        $time_start = $request->get('time_start');
        $time_end = $request->get('time_end');
        if($part_learning->listening==1 )
        {
           $part_learning->time_start=$time_start;
           $part_learning->save();

            $result["data"] = json_decode(json_encode($part_learning), True);
            return json_encode($result);
        }
        if($part_learning->remember==1 or $part_learning[0]->speaking==1  )
        {
            $part_learning->time_end=$time_end;
            $part_learning->save();
            $result["data"] = json_decode(json_encode($part_learning), True);
            return json_encode($result);
        }

        return json_encode($result);
    }
    public function getReport(Request $request)
    {
        if (!isset($request->type) or !isset($request->access_token)){
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $data = array();
        $user_id = $this->checkValidUser($request->access_token);
        if (!$user_id){
            $result['error'] = 101;
            $result['data'] = array();
            $result['data']['message'] = 'invalid token';
            return  $result;
        }
        switch ($request->type) {
            case 1://report by lesson
            {
                //  print_r($query);
                $Lessons = DB::table("user_lesson_study")->where('user_id', $user_id)->get();
                //print_r(count($Lessons));
                for ($i = 0; $i < count($Lessons); $i++) {
                    $lessonname=DB::table("lessons")->where('id', $Lessons[$i]->lesson_id)->select('name')->get();
                    $data["id"] = $Lessons[$i]->id;
                    $data["lesson_id"] = $Lessons[$i]->lesson_id;
                    $data["lesson_name"] = $lessonname[0]->name;
                    $data["type_id"] = $Lessons[$i]->type_id;
                    $data["status_id"] = $Lessons[$i]->status_id;
                    $data["startDate"] = $Lessons[$i]->startDate;
                    $data["endDate"] = $Lessons[$i]->endDate;
                    $data1[$i] = $data;
                }
                $result = array();
                $result["error"] = 0;
               $result["data"] = $data1;
                return json_encode($result);
                break;
            }
            case 2: //report by word
            {
                $Lessons = DB::table("user_lesson_study")->where('user_id', $user_id)
                    ->select('id','word','status_id','startDate','endDate')->get();
                $array_lesson = json_decode(json_encode($Lessons), True);
              //  $data["array_title"] = $array_lesson;

                $result = array();
                $result["error"] = 0;
                $result["data"] = $array_lesson;
                return json_encode($result);
                break;
            }
            case 3: //report by sentence
            {

            }
        }
    }
    private function checkValidUser($access_token){
        $user = DB::table("users")->where("access_token",$access_token)->first();
        if (!isset($user)|| count($user)==0)
            return false;
        $userProfile = json_decode(json_encode($user), True);
        //return user id
        return $userProfile['id'];
    }


}

<?php

namespace App\Http\Controllers\Api;

use App\UserLessonStudy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use App\StudentLessonHistory;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;

class LessonHistoryController extends Controller
{
    public function getListContent()
    {

        $data = array();
        $data1 = array();
        $Content = DB::table("contents")->get();
        // print_r(count($Content));
        for ($i = 0; $i < count($Content); $i++) {
            $data["id"] = $Content[$i]->id;
            $data["name"] = $Content[$i]->name;
            $data["description"] = $Content[$i]->description;
            // get list of chapter

            $chapters = DB::table("chapters")->where('content_id', $Content[$i]->id)->select("id", "name", "number_lesson as nlesson")->get();
            $arrayChapters = json_decode(json_encode($chapters), True);
            for ($j = 0; $j < count($arrayChapters); $j++) {
                $lessons = DB::table("lessons")->where('chapter_id', $chapters[$j]->id)->select("id", "name")->get();
                $arrayLessons = json_decode(json_encode($lessons), True);

                //get list video
                for ($k = 0; $k < count($arrayLessons); $k++) {
                    $videos = DB::table("videos")->where('lesson_id', $lessons[$k]->id)->select("id", "name", "subtitle")->get();
                    $arrayVideos = json_decode(json_encode($videos), True);
                    $arrayLessons[$k]["videos"] = $arrayVideos;
                }
                $arrayChapters[$j]["lessons"] = $arrayLessons;

            }
            $data["chapters"] = $arrayChapters;
            $data1[$i] = $data;

        }

        $result = array();
        $result["error"] = 0;
        $result["data"] = $data1;
        return json_encode($result);

    }
    public function getContentByid(Request $request)
    {
        if (!isset($request->id)){
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $data = array();
        $data1 = array();
        $id=$request->id;
        $Content = DB::table("contents")->where("id",$id)->get();
        // print_r(($Content));

        for ($i = 0; $i < count($Content); $i++) {
            $data["id"] = $Content[$i]->id;
            $data["name"] = $Content[$i]->name;
            $data["description"] = $Content[$i]->description;
            // get list of chapter

            $chapters = DB::table("chapters")->where('content_id', $Content[$i]->id)->select("id", "name", "number_lesson as nlesson")->get();
            $arrayChapters = json_decode(json_encode($chapters), True);
            for ($j = 0; $j < count($arrayChapters); $j++) {
                $lessons = DB::table("lessons")->where('chapter_id', $chapters[$j]->id)->select("id", "name")->get();
                $arrayLessons = json_decode(json_encode($lessons), True);

                //get list video
                for ($k = 0; $k < count($arrayLessons); $k++) {
                    $videos = DB::table("videos")->where('lesson_id', $lessons[$k]->id)->select("id", "name", "subtitle")->get();
                    $arrayVideos = json_decode(json_encode($videos), True);
                    $arrayLessons[$k]["videos"] = $arrayVideos;
                }
                $arrayChapters[$j]["lessons"] = $arrayLessons;

            }
            $data["chapters"] = $arrayChapters;
            $data1[$i] = $data;

        }

        $result = array();
        $result["error"] = 0;
        $result["data"] = $data1;
        return json_encode($result);
    }

    public function getChapterByid(Request $request)
    {
        if (!isset($request->id)){
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $data = array();
        $id=$request->id;

            $chapters = DB::table("chapters")->where('id', $id)->select("id", "name", "number_lesson as nlesson")->get();
            $arrayChapters = json_decode(json_encode($chapters), True);
            for ($j = 0; $j < count($arrayChapters); $j++) {
                $lessons = DB::table("lessons")->where('chapter_id', $chapters[$j]->id)->select("id", "name")->get();
                $arrayLessons = json_decode(json_encode($lessons), True);

                //get list video
                for ($k = 0; $k < count($arrayLessons); $k++) {
                    $videos = DB::table("videos")->where('lesson_id', $lessons[$k]->id)->select("id", "name", "subtitle")->get();
                    $arrayVideos = json_decode(json_encode($videos), True);
                    $arrayLessons[$k]["videos"] = $arrayVideos;
                }
                $arrayChapters[$j]["lessons"] = $arrayLessons;

            }
            $data["chapters"] = $arrayChapters;


        $result = array();
        $result["error"] = 0;
        $result["data"] = $data;
        return json_encode($result);
    }

    public function getWordByLessonID(Request $request)
    {
        if (!isset($request->id)){
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $result['error'] = 0;
        $result['data'] = array();
        try{
        $id=$request->id;
        $lessons = DB::table("lessons")->where('id', $id)->select("word")->get();
      //  $listword=DB::table("words")->where('lesson_id', $lessons[0]->id)->select('word')->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $lessons;
        return json_encode($result);
        }
        catch (Exception $exception){
            return json_encode($result);
        }
    }
    public function getListChapter()
    {
        $chapter=DB::table("chapters")->get();
        $result["data"] = $chapter;
        return json_encode($result);
    }
    public function getListLessonByChapterID(Request $request)
    {
        if (!isset($request->id)){
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $id=$request->id;
        $chapter=DB::table("chapters")->where('id',$id)->select("id","name")->get();
        $listlesson=DB::table("lessons")->where("chapter_id",$chapter[0]->id )->select('id','name')->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $listlesson;
        return json_encode($result);
    }

    public function lessonUpdate(Request $request){
        //checking valid request
       $result = array();

       if (!isset($request->access_token)){
           $result['error'] = 101;
           $result['data'] = array();
           $result['data']['message'] = 'invalid token';
           return  $result;
       }
       $user_id = $this->checkValidUser($request->access_token);
       if (!$user_id){
           $result['error'] = 101;
           $result['data'] = array();
           $result['data']['message'] = 'invalid token';
           return  $result;
       }
       $study = null;

       if ($request->type_id===2){
            $study = DB::table('user_lesson_study')->where("user_id",$user_id)->where("type_id",$request->type_id)
            ->where("lesson_id",$request->lesson_id)->where("word","=",$request->word)->first();
       }else {
           $study = DB::table('user_lesson_study')->where("user_id",$user_id)->where("type_id",$request->type_id)
               ->where("lesson_id",$request->lesson_id)->first();
       }

       $user_lesson = new UserLessonStudy();
       $user_lesson['user_id'] = $user_id;
       $user_lesson['lesson_id']= $request->lesson_id;
       $user_lesson['type_id'] = $request->type_id;
       $user_lesson['status_id'] = $request->status_id;
       $user_lesson['startDate']= $request->startDate;
       $user_lesson['endDate'] = $request->endDate;//empty if not study finish .
       if ($request->type_id===2){
           $user_lesson['word'] = $request->word;
       }
       if (!isset($study) || count($study)===0)
       {
           $user_lesson->save();
       }else {
           //update

           $ul = UserLessonStudy::find($study->id);
           $ul->status_id = $request->status_id;
           $ul->endDate  = $request->endDate;
           $ul->save();

       }
       $result['error'] = 0;
       $result['data'] = array();
       return json_encode($result);
   }
    //get phrase to learn
   public function phraseLearn(Request $request)
   {
       if (!isset($request->lesson_id)) {
           $result['error'] = 100;
           $result['data'] = array();
           $result['data']['message'] = 'Missing parameter';
           return $result;
       }
       $lesson_id=$request->lesson_id;
       $listmp3=DB::table("sentences")->where([['lesson_id','=',$lesson_id],['learn','=','1'],])
           ->select("sentence_jp","sentence_vi","mp3","type","learn")->get();
       $result = array();
       $result["error"] = 0;
       $result["data"] = $listmp3;
       return json_encode($result);
   }
   // get word
    public function listWord(Request $request)
    {
        if (!isset($request->lesson_id))
        {   $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $lesson_id=$request->lesson_id;
        $listword=DB::table("words")->where('lesson_id',$lesson_id)->select("word_jp","word_vi","mp3")->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $listword;
        return json_encode($result);
    }
    //get conversation
    public function conversation(Request $request)
    {
        if (!isset($request->lesson_id)) {
            $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return $result;
        }
        $lesson_id=$request->lesson_id;
        $list=DB::table("sentences")->where([['lesson_id','=',$lesson_id],['type','<>','0'],])
            ->select("sentence_jp","sentence_vi","mp3","type","learn")->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $list;
        return json_encode($result);
    }
    // get list question by lesson
    public function question(Request $request)
    {
        if (!isset($request->lesson_id))
        {   $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $lesson_id=$request->lesson_id;
        $listQuestion=DB::table("questions")->where('lesson_id',$lesson_id)->select("id","question","mp3")->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $listQuestion;
        return json_encode($result);
    }
    // get list question by lesson
    public function answer(Request $request)
    {
        if (!isset($request->question_id))
        {   $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $question_id=$request->question_id;
        $listAnswer=DB::table("answers")->where('question_id',$question_id)->select("answer","yes")->get();
        $result = array();
        $result["error"] = 0;
        $result["data"] = $listAnswer;
        return json_encode($result);
    }
    public function listQA(Request $request)
    {
        if (!isset($request->lesson_id))
        {   $result['error'] = 100;
            $result['data'] = array();
            $result['data']['message'] = 'Missing parameter';
            return  $result;
        }
        $lesson_id=$request->lesson_id;
        $data = array();

        $questions = DB::table("questions")->where('lesson_id',$lesson_id)->select("id","question_vi","question","mp3")->get();
        $arrayQuestions = json_decode(json_encode($questions), True);
        for ($j = 0; $j < count($arrayQuestions); $j++) {
            $answers = DB::table("answers")->where('question_id', $questions[$j]->id)->select("id","answer","yes")->get();
            $arrayAnswers = json_decode(json_encode($answers), True);

            $arrayQuestions[$j]["answers"] = $arrayAnswers;
        }
        $data["questions"] = $arrayQuestions;

        $result = array();
        $result["error"] = 0;
        $result["data"] = $data;
        return json_encode($result);
    }
   private function checkUserLesson($request)
   {
       $study = DB::table('user_lesson_study')->where("user_id",$request["user_id"])->where("type_id",$request["type_id"])
           ->where("lesson_id",$request["lesson_id"])->first();

   }

   private function checkValidUser($access_token){
       $user = DB::table("users")->where("access_token",$access_token)->first();
       if (!isset($user)|| count($user)==0)
           return false;
       $userProfile = json_decode(json_encode($user), True);
       //return user id
       return $userProfile['id'];
   }
}



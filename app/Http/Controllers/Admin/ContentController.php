<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\Chapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;
use DB;

class ContentController extends Controller
{
    public function index()
    {
        $contents = Content::all();
        return view('admin.content.index', compact('contents'));
    }

    public function get(Request $request)
    {
        $id = $request->id;
        try {
            $data = Content::find($id);
            return response()->json(['status' => 'success', 'content' => $data]);
        } catch (Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
        }

    }

    public function detail(Request $request)
    {
        $id = $request->id;
        $detail = Content::find($id);
        $data=$detail->chapter;
      //  return view('admin.content.detail', compact('data'));
        dd($data);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            try {
                $content = new Content();
                $content->name = $request->name;
                $content->description = $request->description;
                //
                $path = "resources/upload/content/";
                if (!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }

                $file_name = $request->file('icon')->getClientOriginalName();
                $content->icon = $file_name;
                $request->file('icon')->move($path, $file_name);

                // $content->icon = $request->icon;
                $content->author = Auth()->user()->id;
                $content->filename = $request->filename;

                try {
                    $content->save();
                    return response()->json(['status' => 'success', 'course' => $content, 'message' => 'Content has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $content = Content::find($request->id);
                $content->name = $request->name;
                $content->description = $request->description;
                //  $content->icon = $request->icon;
                $path = "resources/upload/content/";
                if (!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }

                $file_name = $request->file('icon')->getClientOriginalName();
                $content->icon = $file_name;
                $request->file('icon')->move($path, $file_name);

                $content->filename = $request->filename;

                try {
                    $content->save();
                    return response()->json(['status' => 'success', 'course' => $content, 'message' => 'Content has been updated successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
                }
            } catch (\Exception $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }

    public function delete(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $content = Content::find($request->id);

                try {
                    $content->delete();
                    return response()->json(['status' => 'success', 'content' => $content, 'message' => 'Content has been deleted successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }

}

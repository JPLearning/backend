<?php

namespace App\Http\Controllers\Admin;

use App\Lesson;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    public function index()
    {
        $lessons = Lesson::all();
        $questions = Question::all();
        return view('admin.question.index',compact('lessons','questions'));
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {

                $path="resources/upload/question/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $question = new Question();
                $question->question = $request->question;
                $question->question_vi = $request->question_vi;
                $question->lesson_id = $request->lesson_id;
                //upload file mp3
                $file_name=$request->file('mp3')->getClientOriginalName();
                //  $sentence->mp3 = $file_name;
                $path = $request->file('mp3')->move($path,$file_name);
                $question->mp3 =url($path);


                try {
                    $question->save();
                    return response()->json(['status' => 'success', 'sentence' => $question, 'message' => 'Question has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
}

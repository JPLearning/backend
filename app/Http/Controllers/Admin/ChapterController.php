<?php

namespace App\Http\Controllers\Admin;

use App\Chapter;
use App\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChapterController extends Controller
{
    public function index()
    {

        $chapters = Chapter::all();
        $contents = Content::all();
        return view('admin.chapter.index',compact('chapters','contents'));
    }
    public function get(Request $request)
    {
        $id = $request->id;
        try {
            $data = Chapter::find($id);
            return response()->json(['status' => 'success', 'chapter' => $data]);
        } catch (Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
        }

    }
    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $chapter = Chapter::find($request->id);
                $chapter->name = $request->name;
//                $chapter->icon = $request->icon;
                $path="resources/upload/chapter/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $file_name=$request->file('icon')->getClientOriginalName();
                $chapter->icon = $file_name;
                $request->file('icon')->move($path,$file_name);

                $chapter->content_id = $request->content_id;

                try {
                    $chapter->save();
                    return response()->json(['status' => 'success', 'course' => $chapter, 'message' => 'Chapter has been updated successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
                }
            } catch (\Exception $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
    public function delete(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $chapter = Chapter::find($request->id);

                try{
                    $chapter->delete();
                    return response()->json(['status' => 'success', 'content' => $chapter, 'message' => 'Chapter has been deleted successfully', 'type' => 'success']);
                }
                catch (Exception $exception){
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(),'type'=>'error']);

                }

            }
            catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(),'type'=>'error']);
            }
        }
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $chapter = new Chapter();
                $chapter->name = $request->name;
              //  $chapter->icon = $request->icon;
                $path="resources/upload/chapter/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $file_name=$request->file('icon')->getClientOriginalName();
                $chapter->icon = $file_name;
                $request->file('icon')->move($path,$file_name);

                $chapter->content_id = $request->content_id;

                try {
                    $chapter->save();
                    return response()->json(['status' => 'success', 'chapter' => $chapter, 'message' => 'Chapter has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
}

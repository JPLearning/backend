<?php

namespace App\Http\Controllers\Admin;

use App\Lesson;
use App\Chapter;
use App\Sentence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SentenceController extends Controller
{

    public function index()
    {
        $lessons = Lesson::all();
        $sentences = Sentence::all();
        return view('admin.sentence.index',compact('lessons','sentences'));
    }
    public function get(Request $request)
    {
        $id = $request->id;
        try {
            $data = Sentence::find($id);
            return response()->json(['status' => 'success', 'sentence' => $data]);
        } catch (Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
        }

    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {

                $path="resources/upload/lesson/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $sentence = new Sentence();
                $sentence->lesson_id = $request->lesson_id;
                $sentence->sentence_jp = $request->sentence_jp;
                $sentence->sentence_vi = $request->sentence_vi;
                $sentence->type = $request->type;
                $sentence->learn = $request->learn;
                //upload file mp3
                $file_name=$request->file('mp3')->getClientOriginalName();
              //  $sentence->mp3 = $file_name;
                $path = $request->file('mp3')->move($path,$file_name);
                $sentence->mp3 =url($path);


                try {
                    $sentence->save();
                    return response()->json(['status' => 'success', 'sentence' => $sentence, 'message' => 'Setence has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $sentence = Sentence::find($request->id);
                $path="resources/upload/lesson/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $sentence->lesson_id = $request->lesson_id;
                $sentence->sentence_jp = $request->sentence_jp;
                $sentence->sentence_vi = $request->sentence_vi;
                $sentence->type = $request->type;
                $sentence->learn = $request->learn;
                //upload file mp3
                $file_name=$request->file('mp3')->getClientOriginalName();
                //  $sentence->mp3 = $file_name;
                $path = $request->file('mp3')->move($path,$file_name);
                $sentence->mp3 =url($path);
                try {
                    $sentence->save();
                    return response()->json(['status' => 'success', 'sentence' => $sentence, 'message' => 'Sentence has been updated successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
                }
            } catch (\Exception $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }


}

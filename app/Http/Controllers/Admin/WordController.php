<?php

namespace App\Http\Controllers\Admin;

use App\Lesson;
use App\Word;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WordController extends Controller
{

    public function index()
    {
        $lessons = Lesson::all();
        $words = Word::all();
        return view('admin.word.index',compact('lessons','words'));
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $path="resources/upload/lesson/word";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                $word = new Word();
                $word->lesson_id = $request->lesson_id;
                $word->word_jp = $request->word_jp;
                $word->word_vi = $request->word_vi;
                //upload file mp3
                $file_name=$request->file('mp3')->getClientOriginalName();
              //  $sentence->mp3 = $file_name;
                $path = $request->file('mp3')->move($path,$file_name);
                $word->mp3 = url($path);

                try {
                    $word->save();
                    return response()->json(['status' => 'success', 'word' => $word, 'message' => 'Word has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }


}

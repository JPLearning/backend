<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AnswerController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        $answers = Answer::all();
        return view('admin.answer.index',compact('answers','questions'));
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $answer = new Answer();
                $answer->answer = $request->answer;
                $answer->question_id = $request->question_id;
                $answer->yes = $request->yes;


                try {
                    $answer->save();
                    return response()->json(['status' => 'success', 'sentence' => $answer, 'message' => 'Question has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
}

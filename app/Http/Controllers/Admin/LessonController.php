<?php

namespace App\Http\Controllers\Admin;

use App\Lesson;
use App\Chapter;
use App\Sentence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class LessonController extends Controller
{
    public function index()
    {
        $lessons = Lesson::all();
        $chapters = Chapter::all();
        return view('admin.lesson.index',compact('lessons','chapters'));
    }
    public function get(Request $request)
    {
        $id = $request->id;
        try {
            $data = Lesson::find($id);
            return response()->json(['status' => 'success', 'lesson' => $data]);
        } catch (Exception $exception) {
            return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
        }

    }
    public function delete(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $lesson = Lesson::find($request->id);

                try{
                    $lesson->delete();
                    return response()->json(['status' => 'success', 'lesson' => $lesson, 'message' => 'Lesson has been deleted successfully', 'type' => 'success']);
                }
                catch (Exception $exception){
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(),'type'=>'error']);

                }

            }
            catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(),'type'=>'error']);
            }
        }
    }
    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $lesson = new Lesson();
                $lesson->name = $request->name;
                $lesson->chapter_id = $request->chapter_id;
                $path="resources/upload/lesson/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                // file mp4
//
//                $file_name=$request->file('video')->getClientOriginalName();
//                $lesson->video = $file_name;
//                $request->file('video')->move($path,$file_name);
//                //file srt
//                $file_name1=$request->file('sub')->getClientOriginalName();
//                $lesson->sub = $file_name1;
//                $request->file('sub')->move($path,$file_name1);
//                //file word txt
//                $file_name2=$request->file('word')->getClientOriginalName();
//                $lesson->word = $file_name2;
//                $request->file('word')->move($path,$file_name2);
//                //file phrase txt
//                $file_name3=$request->file('phrase')->getClientOriginalName();
//                $lesson->phrase = $file_name3;
//                $request->file('phrase')->move($path,$file_name3);
                // store with other table
                $sentence = new Sentence();
                $sentence->lesson_id = $lesson->id;
                $sentence->sentence = $request->sentence;
                $sentence->type = $request->type;
                //upload file mp3
                $file_name=$request->file('mp3')->getClientOriginalName();
                $sentence->mp3 = $file_name;
                $path = $request->file('mp3')->move($path,$file_name);
                $sentence->mp3 = $path;

                try {
                    $lesson->save();
                    $lesson->sentence()->save($sentence);
                    return response()->json(['status' => 'success', 'lesson' => $lesson, 'message' => 'Lesson has been created successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);

                }

            } catch (\Exeption $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            try {
                $lesson = Lesson::find($request->id);
                $lesson->name = $request->name;
                $path="resources/upload/lesson/";
                if(!(is_dir($path))) {
                    mkdir($path, 0777);
                    chmod($path, 0777);
                }
                // file mp4
                $file_name=$request->file('video')->getClientOriginalName();
                $lesson->video = $file_name;
                $request->file('video')->move($path,$file_name);
                //file srt
                $file_name1=$request->file('sub')->getClientOriginalName();
                $lesson->sub = $file_name1;
                $request->file('sub')->move($path,$file_name1);
                //file word txt
                $file_name2=$request->file('word')->getClientOriginalName();
                $lesson->word = $file_name2;
                $request->file('word')->move($path,$file_name2);
                //file phrase txt
                $file_name3=$request->file('phrase')->getClientOriginalName();
                $lesson->phrase = $file_name3;
                $request->file('phrase')->move($path,$file_name3);
                $lesson->chapter_id = $request->chapter_id;
                try {
                    $lesson->save();
                    return response()->json(['status' => 'success', 'lesson' => $lesson, 'message' => 'Chapter has been updated successfully', 'type' => 'success']);
                } catch (Exception $exception) {
                    return response()->json(['status' => 'error', 'message' => $exception->getMessage(), 'type' => 'error']);
                }
            } catch (\Exception $ex) {
                return response()->json(['status' => 'error', 'message' => $ex->getMessage(), 'type' => 'error']);
            }
        }
    }
    //



}

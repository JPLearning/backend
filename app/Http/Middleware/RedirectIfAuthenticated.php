<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::user()->role_id == 1)
                return Redirect("admin");
            else if (Auth::user()->role_id == 2)
                return Redirect("teacher");
            else if (Auth::user()->role_id == 3)
                return Redirect("student");
        }

        return $next($request);
    }
}

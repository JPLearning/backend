<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Sentence extends Model
{
    use Notifiable;
    protected $table = 'sentences';
    public $timestamps = false;
    protected $fillable = ['sentence_jp','sentence_vi','mp3','type','learn','lesson_id'];
    //
    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->updated_at = $model->freshTimestamp();
        });
    }
}

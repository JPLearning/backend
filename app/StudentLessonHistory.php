<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class StudentLessonHistory extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'student_lesson_history';
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'lesson_id', 'status'
    ];

}

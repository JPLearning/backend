<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
class Word extends Model
{
    use Notifiable;
    protected $table = 'words';
    public $timestamps = false;
    protected $fillable = ['word_jp','word_vi','mp3','lesson_id'];
    //
    public function lesson(){
        return $this->belongsTo('App\Lesson');
    }
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
        static::updating(function ($model) {
            $model->updated_at = $model->freshTimestamp();
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Answer extends Model
{
    protected $table = 'answers';
    public $timestamps = false;
    protected $fillable = ['question_id','answer','yes'];
    public function question(){
        return $this->belongsTo('App\Question');
    }

}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">API</div>

                <div class="panel-body">

                    <a href="{{url('api/v1/login')}}">
                        <i class="fa fa-product-hunt"></i> <span>Login</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/lessonUpdate')}}">
                        <i class="fa fa-product-hunt"></i> <span>lessonUpdate</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getReport')}}">
                        <i class="fa fa-product-hunt"></i> <span>getReport</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    </a>
                    <a href="{{url('api/v1/getListContent')}}">
                        <i class="fa fa-product-hunt"></i> <span>getListContent</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getContentByid')}}">
                        <i class="fa fa-product-hunt"></i> <span>getContentByid</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getChapterByid')}}">
                        <i class="fa fa-product-hunt"></i> <span>getChapterByid</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getWordByLessonID')}}">
                        <i class="fa fa-product-hunt"></i> <span>getWordByLessonID</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getListChapter')}}">
                        <i class="fa fa-product-hunt"></i> <span>getListChapter</span>
                        <span class="pull-right-container"></span>
                    </a></br>
                    <a href="{{url('api/v1/getListLessonByChapterID')}}">
                        <i class="fa fa-product-hunt"></i> <span>getListLessonByChapterID</span>
                        <span class="pull-right-container"></span>
                    </a></br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

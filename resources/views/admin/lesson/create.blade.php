<div class="modal fade" id="modalCreateLesson">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <input type="hidden" id="user_id">
                <h4 class="modal-title" id="contentTitle">Create new lesson</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="form-control-label">Lesson Name:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="name" type="text" class="form-control" placeholder="Enter content name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="sentence" class="form-control-label">Sentence:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="sentence" type="text" class="form-control" placeholder="Enter an sentence">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mp3" class="form-control-label">Mp3(.mp3):</label>
                        <input type="file" name="file" id="mp3Input" accept=".mp3"/>
                        <input type="file" id="mp3Input" class="text-center" accept=".mp3"
                               style="display:none" onchange="loadMP3(event)">
                        </label>
                </div>
                <div class="form-group">
                    <label for="type" class="form-control-label">Type:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="type" type="text" class="form-control" placeholder="Enter an type">
                    </div>
                </div>
                <div> <button id="btnAdd" type="button" class="btn btn-primary">Add</button></div>
                {{--<div class="form-group">--}}
                {{--<label for="video" class="form-control-label">Video(.mp4):</label>--}}
                {{--<div class="input-group">--}}
                {{--<span class="input-group-addon"><i class="fa fa-user"></i></span>--}}
                {{--<input id="description" type="text" class="form-control" placeholder="Enter an description">--}}

                {{--<input type="file" name="file" id="videoInput" accept="video/mp4"/>--}}
                {{--<input type="file" id="videoInput" class="text-center" accept="video/mp4" style="display:none" onchange="loadVideo(event)">--}}
                {{--</label>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label for="subInput" class="form-control-label">Sub(.srt):</label>--}}
                {{--<input type="file" name="file" id="subInput" accept=".srt"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label for="wordInput" class="form-control-label">Word(.txt):</label>--}}
                {{--<input type="file" name="file" id="wordInput" accept="text/plain"/>--}}
                {{--</div>--}}
                {{--<div class="form-group">--}}
                {{--<label for="phraseInput" class="form-control-label">Phrase(.txt):</label>--}}
                {{--<input type="file" name="file" id="phraseInput" accept="text/plain"/>--}}
                {{--</div>--}}
                <div class="form-group">
                    <label for="chapter_id" class="form-control-label">Chapter name:</label>
                    <select class="form-control input-sm" name="content_name" id="chapter_id">
                        @foreach($chapters as $key=>$chapter)
                            <option value="{{$chapter->id}}">{{ $chapter->name}}     </option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="btnCreateLesson" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<script>

</script>
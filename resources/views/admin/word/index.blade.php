@section('styles')
    <style>
        .middle {
            vertical-align: middle !important;
        }

        .box {
            overflow-x: scroll;
        }
    </style>
@endsection
@extends('admin.common.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div id="loader" class="loading" hidden>Loading&#8230;</div>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="user_count">123</h3>
                            <p>Totals</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-people"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="free_account">1212</h3>
                            <p>Free account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 id="pro_account">1212</h3>
                            <p>Pro account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>
                            <p>Inactive</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsdown"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-primary" id="createWord"><i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_words" class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Word Japanese</th>
                            <th class="text-center">Word Vietnamese</th>
                            <th class="text-center">Mp3</th>
                            <th class="text-center">Lesson</th>
                            <th class="text-center">Date created</th>
                            <th class="text-center">Date updated</th>
                            <th class="text-center">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($words as $key=>$word)
                            <tr id="{!! $word['id'] !!}">
                                <td class="text-center middle">{!! $key+1 !!}</td>
                                <td class="middle"><a href="{{url('admin/sentences')}}">{!! $word->word_jp !!}</a></td>
                                <td class="middle">{!! $word->word_vi!!}</td>
                                <td class="middle">{!! $word->mp3 !!}</td>
                                <td class="middle">{!! $word->Lesson->name !!}</td>
                                <td class="middle">{!! $word->created_at!!}</td>
                                <td class="middle">{!! $word->updated_at !!}</td>
                                <td class="text-center middle">
                                    <a class="btnEdit btn btn-info btn-sm">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <a class="btnDelete btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.word.create')
    <!--edit leson-->
    <div class="modal fade" id="modalEdit" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <input type="hidden" id="word_id_edit">
                    <h4 class="modal-title" id="user_name"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="word_jp_InputEdit" class="form-control-label">Word Japanese:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="word_jp_InputEdit" type="text" class="form-control" placeholder="Enter word Japanese">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="word_vi_InputEdit" class="form-control-label">Word Vietnamese:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="word_vi_InputEdit" type="text" class="form-control" placeholder="Enter word Vietnamese">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="mp3InputEdit" class="form-control-label">File mp3(.mp3):</label>
                        <input type="file" name="file" id="mp3InputEdit" accept="mp3"/>
                    </div>
                    <div class="form-group">
                        <label for="chapter_idEdit" class="form-control-label">Chapter name:</label>
                        <select class="form-control input-sm" name="content_name" id="chapter_idEdit">
                            @foreach($lessons as $key=>$lesson)
                                <option value="{{$lesson->id}}">{{ $lesson->name}}     </option>
                            @endforeach
                        </select>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="btnSave" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('/public/libs/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/public/libs/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#tbl_lessons').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });

        function Alert(title, message, type) {
            swal({
                title: title,
                text: message,
                type: type,
                showConfirmButton: true
            });
        }

        $("#createWord").click(function () {
            $("#modalCreateWord").modal();
        });

        $(".btnDelete").click(function () {
            var id = $(this).closest('tr').attr('id');
            swal({
                    title: "Are you sure?",
                    text: "Do you really want to delete this lesson!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function () {
                    setTimeout(function () {
                        deleteLeson(id);
                    }, 1000);
                });
        });

        $(".btnEdit").click(function () {
            var id = $(this).closest('tr').attr('id');
            getLesson(id);
            $("#modalEdit").modal();
        });

        $("#btnSave").click(function () {
            var id = $("#lesson_id_edit").val();
            var name = $("#nameInputEdit").val();
            var video = $("#videoInputEdit").val();
            var sub = $("#subInputEdit").val();
            var word = $("#wordInputEdit").val();
            var phrase = $("#phraseInputEditdit").val();
            var chapter_id = $("#chapter_idEdit").val();
            if (name == "") {
                $("#nameInputEdit").focus();
                Alert('Please enter name', '', 'error');
                return;
            }
            if (video == "") {
                $("#videoInputEdit").focus();
                Alert('Please select file video', '', 'error');
                return;
            }
            if (sub == "") {
                $("#subInputEdit").focus();
                Alert('Please select file subtitle', '', 'error');
                return;
            }
            if (word == "") {
                $("#wordInputEdit").focus();
                Alert('Please select file word', '', 'error');
                return;
            }
            if (phrase == "") {
                $("#phraseInputEdit").focus();
                Alert('Please select file phrase', '', 'error');
                return;
            }
            if (chapter_id == "") {
                $("#chapter_idEdit").focus();
                Alert('Please select chapter', '', 'error');
                return;
            }
            updateChapter(id, name, video, sub, word, phrase, chapter_id);
        });

        function getLesson(id) {
            $.ajax({
                data: {id: id, _token: '{!! csrf_token() !!}'},
                url: '{{ route('admin_lesson_get') }}',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $("#lesson_id_edit").val(data.lesson.id);
                    $("#nameInputEdit").val(data.lesson.name);
                    $("#videoInputEdit").val(data.lesson.video);
                    $("#subInputEdit").val(data.lesson.sub);
                    $("#wordInputEdit").val(data.lesson.word);
                    $("#phraseInputEdit").val(data.lesson.phase);
                    $("#chapter_idEdit").val(data.lesson.chapter_id);
                    $("#modalEdit").modal();
                }
            });
        }

        function updateLesson(id, name, video, sub, word, phrase, chapter_id) {
            $("#loader").show();
            var data = new FormData();
            var video_id = $('#videoInputEdit').prop('files')[0];
            var sub_id = $('#subInputEdit').prop('files')[0];
            var word_id = $('#wordInputEdit').prop('files')[0];
            var phrase_id = $('#phraseInputEdit').prop('files')[0];
            data.append('id', id)
            data.append('name', name);
            data.append('video', video_id);
            data.append('sub', sub_id);
            data.append('word', word_id);
            data.append('phrase', phrase_id);
            data.append('chapter_id', chapter_id);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_lesson_update') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalEdit").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });
        }

        function deleteLeson(id) {
            $.ajax({
                type: 'POST',
                url: '{!! route('admin_lesson_delete') !!}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#" + id).remove();
                    }
                    else {
                        Alert(data.status, data.message, data.type);

                    }
                },
                error: function (data) {
                    Alert('Something went wrong', '', 'error');
                }
            });
        }


        $("#btnCreateWord").click(function () {
            var word_jp = $("#word_jp").val();
            var word_vi = $("#word_vi").val();
            var mp3 = $("#mp3Input").val();
            var lesson_id = $("#lesson_id").val();

            if (word_jp == "") {
                $("#word_jp").focus();
                Alert('Please enter Japanese word', '', 'error');
                return;
            }
            if (word_vi == "") {
                $("#word_vi").focus();
                Alert('Please enter Vietnamese word', '', 'error');
                return;
            }

                if (mp3 == "") {
                    $("#mp3Input").focus();
                    Alert('Please select file mp3', '', 'error');
                    return;
            }
            if (lesson_id == "") {
                $("#lesson_id").focus();
                Alert('Select lesson name', '', 'error');
                return;
            }

            createWord( word_jp,word_vi, mp3,lesson_id);

        });

        function createWord(word_jp,word_vi, mp3, lesson_id) {
            $("#loader").show();

            var data = new FormData();
            var mp3_id = $('#mp3Input').prop('files')[0];
            data.append('word_jp', word_jp);
            data.append('word_vi', word_vi);
            data.append('mp3', mp3_id);
//            data.append('video', video_id);
//            data.append('sub', sub_id);
//            data.append('word', word_id);
//            data.append('phrase', phrase_id);
            data.append('lesson_id', lesson_id);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_word_create') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
//                        $("#btnCreateWord").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });
        }


    </script>
@endsection
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="{{(Request::is('admin/dashboard/*')||Request::is('admin/dashboard'))?'active':''}}">
                <a href="">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

            <li class="{{(Request::is('admin/users/*')||Request::is('admin/users')||Request::is('admin/user/*'))?'active':''}}">
                <a href="">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

            <li class="{{(Request::is('admin/projects/*')||Request::is('admin/projects'))?'active':''}}">
                <a href="{{url('admin/contents')}}">
                    <i class="fa fa-folder-o"></i> <span>Contents</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>

            <li class="{{(Request::is('admin/sales/*')||Request::is('admin/sales'))?'active':''}}">
                <a href="{{url('admin/chapters')}}">
                    <i class="fa fa-list-alt"></i> <span>Chapters</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>

            <li class="{{(Request::is('admin/surveys/*')||Request::is('admin/surveys'))?'active':''}}">
                <a href="{{url('admin/lessons')}}">
                    <i class="fa fa-book"></i> <span>Lessons</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{(Request::is('admin/surveys/*')||Request::is('admin/surveys'))?'active':''}}">
                <a href="{{url('admin/sentences')}}">
                    <i class="fa fa-book"></i> <span>Sentences</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{(Request::is('admin/surveys/*')||Request::is('admin/surveys'))?'active':''}}">
                <a href="{{url('admin/words')}}">
                    <i class="fa fa-book"></i> <span>Words</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{(Request::is('admin/surveys/*')||Request::is('admin/surveys'))?'active':''}}">
                <a href="{{url('admin/questions')}}">
                    <i class="fa fa-book"></i> <span>Questions</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="{{(Request::is('admin/surveys/*')||Request::is('admin/surveys'))?'active':''}}">
                <a href="{{url('admin/answers')}}">
                    <i class="fa fa-book"></i> <span>Answers</span>
                    <span class="pull-right-container">
            </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-code"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a id="resetDatabase" href="#"><i class="fa fa-refresh text-red"></i> Reset database</a></li>
                </ul>
            </li>
            <li class="header">LOGS</li>
            <li class="{{(Request::is('admin/logs/*')||Request::is('admin/logs'))?'active':''}}">
                <a href=""><i class="fa fa-archive"></i> <span>View logs</span></a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
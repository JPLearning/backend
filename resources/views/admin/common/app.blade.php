<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title or 'JPlearning'}}</title>
    {{--<script>--}}
        {{--window.Laravel = {!! json_encode([--}}
            {{--'csrfToken' => csrf_token(),--}}
        {{--]) !!}';--}}
    {{--</script>--}}
    {{--<link rel="shortcut icon" href="/img/favicon.ico">--}}
    @include('admin.common.style')
    @yield('styles')

    {{--<script type="text/javascript" src="{{ url('admin/js/ckeditor/ckeditor.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ url('admin/js/ckfinder/ckfinder.js') }}"></script>--}}
    {{--<script type="text/javascript">  var baseURL= "{!! url('/') !!}";</script>--}}
    {{--<script type="text/javascript" src="{{ url('admin/js/func_ckfinder.js') }}"></script>--}}
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

    @include('admin.common.header')
    @include('admin.common.sidebar')

    @yield('content')

    @include('admin.common.footer')


</div>
@include('admin.common.script')
@yield('scripts')
</body>
{{--<script type="text/javascript" src="{{ url('admin/js/myscript.js') }}"></script>--}}
</html>
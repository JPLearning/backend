@section('styles')
    <style>
        .middle {
            vertical-align: middle !important;
        }

        .box {
            overflow-x: scroll;
        }
    </style>
@endsection
@extends('admin.common.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div id="loader" class="loading" hidden>Loading&#8230;</div>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="user_count">123</h3>
                            <p>Totals</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-people"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="free_account">1212</h3>
                            <p>Free account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 id="pro_account">1212</h3>
                            <p>Pro account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>
                            <p>Inactive</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsdown"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-primary" id="createChapter"><i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_chapters" class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Chapter name</th>
                            <th class="text-center">Icon</th>
                            <th class="text-center">Number lesson</th>
                            <th class="text-center">Date created</th>
                            <th class="text-center">Date updated</th>
                            <th class="text-center">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key=>$data1)
                            <tr id="{!! $data1['id'] !!}">
                                <td class="text-center middle">{!! $key+1 !!}</td>
                                <td class="middle"><a href="{{url('admin/contents')}}">{!! $data1->name !!}</a></td>
                                <td class="middle"><img src="{!! asset('resources/upload/chapter/'.$data1->icon)!!}" alt=""
                                                        height="40" width="40"></td>
                                <td class="middle">{!! count($data1->number_lesson)!!}</td>
                                <td class="middle">{!! $data1->created_at!!}</td>
                                <td class="middle">{!! $data1->updated_at !!}</td>
                                <td class="text-center middle">
                                    <a class="btnEdit btn btn-info btn-sm">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <a class="btnDelete btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.chapter.create')
    <!--edit chapter-->
    <div class="modal fade" id="modalEdit" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <input type="hidden" id="chapter_id_edit">
                    <h4 class="modal-title" id="user_name"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="form-control-label">Chapter name:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="nameEdit" type="text" class="form-control" placeholder="Enter a chapter name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="icon" class="form-control-label">Icon:</label>
                        {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-pencil"></i></span>--}}
                            {{--<textarea id="iconEdit" type="text" class="form-control"--}}
                                      {{--placeholder="Select icon"></textarea>--}}
                        {{--</div>--}}
                        <p class="text-center">
                            <img src="" width="100px" height="100px" id="icon_update">
                        </p>
                        <p class="">
                            <label for="iconInput" style="padding-top: 10px;cursor: hand">
                                <a>
                                    <span class="glyphicon glyphicon-folder-open text-dapp" aria-hidden="true"></span>&nbsp;
                                    Choose an image
                                </a>
                                <input type="file" id="iconInput" class="text-center" accept="image/*" style="display:none" onchange="loadIcon(event)">
                            </label>
                        </p>

                    </div>
                    <div class="form-group">
                        <label for="content_id" class="form-control-label">Content name:</label>
                        <select class="form-control input-sm" name="content_name" id="content_id_edit">
                            @foreach($contents as $key=>$content)
                                <option value="{{$content->id}}">{{ $content->name}}     </option>
                            @endforeach
                        </select>
                    </div>
                    {{--<div class="form-group">--}}
                    {{--<label for="description" class="form-control-label">Content name:</label>--}}
                    {{--<div class="input-group">--}}
                    {{--<span class="input-group-addon"><i class="fa fa-user"></i></span>--}}
                    {{--<input id="content_id_Edit" type="text" class="form-control" placeholder="Select an description">--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="btnSave" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('/public/libs/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/public/libs/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#tbl_chapters').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });

        function Alert(title, message, type) {
            swal({
                title: title,
                text: message,
                type: type,
                showConfirmButton: true
            });
        }

        var loadIcon = function (event) {
            var fileSize = $('#iconInput').prop('files')[0].size;
            var fileType = $('#iconInput').prop('files')[0].type;
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) > 0) {
                if (fileSize > 2097152) {
                    swal("Warning", 'Image size too large, accept image with under 2 Mb only', 'warning');
                    $('#iconInput').replaceWith(input.val('').clone(true));
                    return false;
                }
                else {
                    var photo_id = document.getElementById('icon_update');
                    photo_id.src = URL.createObjectURL(event.target.files[0]);
                    return true;
                }
            }
            else {
                $('#iconInput').replaceWith(input.val('').clone(true));
                swal("Oops!", 'Please choose an image with *.png, *.jpg format', 'warning');
                return false;
            }
        }

        $("#createChapter").click(function () {
            $("#modalCreateChapter").modal();
        });

        $(".btnDelete").click(function () {
            var id = $(this).closest('tr').attr('id');
            swal({
                    title: "Are you sure?",
                    text: "Do you really want to delete this chapter!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function () {
                    setTimeout(function () {
                        deleteChapter(id);
                    }, 1000);
                });
        });

        $(".btnEdit").click(function () {
            var id = $(this).closest('tr').attr('id');
            getChapter(id);
            $("#modalEdit").modal();
        });

        $("#btnSave").click(function () {
            var id = $("#chapter_id_edit").val();
            var name = $("#nameEdit").val();
            var icon = $("#iconInput").val();
            var content_id = $("#content_id_edit").val();
            if (name == "") {
                $("#nameEdit").focus();
                Alert('Please enter name', '', 'error');
                return;
            }
//            if (icon == "") {
//                $("#iconEdit").focus();
//                Alert('Please select icon', '', 'error');
//                return;
//            }
            if (content_id == "") {
                $("#content_id_edit").focus();
                Alert('Please select content', '', 'error');
                return;
            }
            updateChapter(id, name, icon, content_id);
        });

        function getChapter(id) {
            $.ajax({
                data: {id: id, _token: '{!! csrf_token() !!}'},
                url: '{{ route('admin_chapter_get') }}',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    $("#chapter_id_edit").val(data.chapter.id);
                    $("#nameEdit").val(data.chapter.name);
//                    $("#iconEditInput").val(data.chapter.icon);
//                   $("#content_id_Edit").val(data.chapter.content_id);
                    $("#modalEdit").modal();
                }
            });
        }

        function updateChapter(id, name, icon, content_id) {
            $("#loader").show();
            var data = new FormData();
            var photo_id = $('#iconInput').prop('files')[0];
            data.append('id', id);
            data.append('name', name);
            data.append('icon', photo_id);
            data.append('content_id', content_id);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_chapter_update') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalEdit").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });
        }

        function deleteChapter(id) {
            $.ajax({
                type: 'POST',
                url: '{!! route('admin_chapter_delete') !!}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#" + id).remove();
                    }
                    else {
                        Alert(data.status, data.message, data.type);

                    }
                },
                error: function (data) {
                    Alert('Something went wrong', '', 'error');
                }
            });
        }


        $("#btnCreateChapter").click(function () {
            var name = $("#name").val();
            var content_id = $("#content_id").val();
            var icon = $("#icon").val();

            if (name == "") {
                $("#name").focus();
                Alert('Please enter name', '', 'error');
                return;
            }
            if (icon == "") {
                $("#icon").focus();
                Alert('Please select icon', '', 'error');
                return;
            }
            if (content_id == "") {
                $("#content_id").focus();
                Alert('Please select content', '', 'error');
                return;
            }

            createChapter(name, icon, content_id);

        });

        function createChapter(name, icon, content_id) {
            $("#loader").show();

            var data = new FormData();
            var photo_id = $('#photo_id_input').prop('files')[0];
            data.append('name', name);
            data.append('icon', photo_id);
            data.append('content_id', content_id);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_chapter_create') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalCreateChapter").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });

        }


    </script>
@endsection
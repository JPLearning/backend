@section('styles')
    <style>
        .middle {
            vertical-align: middle !important;
        }

        .box {
            overflow-x: scroll;
        }
    </style>
@endsection
@extends('admin.common.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div id="loader" class="loading" hidden>Loading&#8230;</div>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="user_count">123</h3>
                            <p>Totals</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-people"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="free_account">1212</h3>
                            <p>Free account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 id="pro_account">1212</h3>
                            <p>Pro account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>
                            <p>Inactive</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsdown"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-primary" id="createContent"><i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_contents" class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Content name</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Icon</th>
                            <th class="text-center">Number chapter</th>
                            <th class="text-center">Author</th>
                            <th class="text-center">Date created</th>
                            <th class="text-center">Date updated</th>
                            <th class="text-center">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contents as $key=>$content)
                            <tr id="{!! $content['id'] !!}">
                                <td class="text-center middle">{!! $key+1 !!}</td>
                                <td class="middle"><a href="{{url('admin/content/{id}')}}">{!! $content->name !!}</a></td>
                                <td class="middle">{!! $content->description !!}</td>
                                <td class="middle"><img src="{!! asset('resources/upload/content/'.$content->icon)!!}" alt=""
                                                        height="40" width="40"></td>
                                <td class="middle">{!!count($content->chapter)!!}</td>
                                <td class="middle">{!! $content->User->name !!}</td>
                                <td class="middle">{!! $content->created_at!!}</td>
                                <td class="middle">{!! $content->updated_at !!}</td>
                                <td class="text-center middle">
                                    <a class="btnEdit btn btn-info btn-sm">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <a class="btnDelete btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.content.create')
    <!--edit content-->
    <div class="modal fade" id="modalEdit" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <input type="hidden" id="content_id_edit">
                    <h4 class="modal-title" id="user_name"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class="form-control-label">Content name:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="nameEdit" type="text" class="form-control" placeholder="Enter a content name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descriptionEdit" class="form-control-label">Description:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="descriptionEdit" type="text" class="form-control"
                                   placeholder="Enter an description">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="icon" class="form-control-label">Icon:</label>
                        {{--<div class="input-group">--}}
                            {{--<span class="input-group-addon"><i class="fa fa-pencil"></i></span>--}}
                            {{--<textarea id="iconEdit" type="text" class="form-control"--}}
                                      {{--placeholder="Select icon"></textarea>--}}
                            <p class="text-center">
                                <img src="" width="100px" height="100px" id="icon_id_update">
                            </p>
                            <p class="">
                                <label for="iconEditInput" style="padding-top: 10px;cursor: hand">
                                    <a>
                                        <span class="glyphicon glyphicon-folder-open text-dapp" aria-hidden="true"></span>&nbsp;
                                        Choose an image
                                    </a>
                                    <input type="file" id="iconEditInput" class="text-center" accept="image/*" style="display:none" onchange="loadIcon(event)">
                                </label>
                            </p>
                        {{--</div>--}}
                    </div>
                    <div class="form-group">
                        <label for="filename" class="form-control-label">Filename:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <textarea id="filenameEdit" type="text" class="form-control"
                                      placeholder="Enter filename"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="btnSave" type="button" class="btn btn-primary">Save</button>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('/public/libs/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/public/libs/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#tbl_contents').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });
        function Alert(title, message, type) {
            swal({
                title: title,
                text: message,
                type: type,
                showConfirmButton: true
            });
        }
        var loadIcon = function (event) {
            var fileSize = $('#iconEditInput').prop('files')[0].size;
            var fileType = $('#iconEditInput').prop('files')[0].type;
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) > 0) {
                if (fileSize > 2097152) {
                    swal("Warning", 'Image size too large, accept image with under 2 Mb only', 'warning');
                    $('#iconEditInput').replaceWith(input.val('').clone(true));
                    return false;
                }
                else {
                    var photo_id = document.getElementById('icon_id_update');
                    photo_id.src = URL.createObjectURL(event.target.files[0]);
                    return true;
                }
            }
            else {
                $('#iconEditInput').replaceWith(input.val('').clone(true));
                swal("Oops!", 'Please choose an image with *.png, *.jpg format', 'warning');
                return false;
            }
        }
        $("#createContent").click(function () {
            $("#modalCreateContent").modal();
        });

        $(".btnDelete").click(function () {
            var id = $(this).closest('tr').attr('id');
            swal({
                    title: "Are you sure?",
                    text: "Do you really want to delete this content!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function () {
                    setTimeout(function () {
                        deleteContent(id);
                    }, 1000);
                });
        });

        $(".btnEdit").click(function () {
            var id = $(this).closest('tr').attr('id');
            getContent(id);
            $("#modalEdit").modal();
        });

        $("#btnSave").click(function () {
            var id = $("#content_id_edit").val();
            var name = $("#nameEdit").val();
            var description = $("#descriptionEdit").val();
            var icon = $("#iconEditInput").val();
            var filename = $("#filenameEdit").val();

            if (name == "") {
                $("#nameEdit").focus();
                Alert('Please enter name', '', 'error');
                return;
            }
            if (description == "") {
                $("#descriptionEdit").focus();
                Alert('Please enter  description', '', 'error');
                return;
            }
            if (icon == "") {
                $("#iconEditInput").focus();
                Alert('Please select  icon', '', 'error');
                return;
            }
            if (filename == "") {
                $("#filenameEdit").focus();
                Alert('Please enter  filename', '', 'error');
                return;
            }


            updateContent(id, name, description, icon, filename);
        });

        function getContent(id) {
            $.ajax({
                data: {id: id, _token: '{!! csrf_token() !!}'},
                url: '{{ route('admin_content_get') }}',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $("#content_id_edit").val(data.content.id);
                    $("#nameEdit").val(data.content.name);
                    $("#descriptionEdit").val(data.content.description);
//                    $("#iconEdit").val(data.content.icon);
                    $("#iconEdit").attr('src', '/resources/upload/'+data.content.icon);
                    $("#filenameEdit").val(data.content.filename);
                    $("#modalEdit").modal();
                }
            });
        }

        function updateContent(id, name, description, icon, filename) {
            $("#loader").show();

            var data = new FormData();
            var photo_id = $('#iconEditInput').prop('files')[0];
            data.append('id', id);
            data.append('name', name);
            data.append('description', description);
            data.append('icon', photo_id);
            data.append('filename', filename);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_content_update') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalEdit").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });
        }

        function deleteContent(id) {
            $.ajax({
                type: 'POST',
                url: '{!! route('admin_content_delete') !!}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#" + id).remove();
                    }
                    else {
                        Alert(data.status, data.message, data.type);

                    }
                },
                error: function (data) {
                    Alert('Something went wrong', '', 'error');
                }
            });
        }


        $("#btnCreateContent").click(function () {
            var name = $("#name").val();
            var description = $("#description").val();
            var icon = $("#photo_id_input").val();
            var filename = $("#filename").val();
//            var number_chapter = $("#number_chapter").val();

            if (name == "") {
                $("#name").focus();
                Alert('Please enter name', '', 'error');
                return;
            }
            if (description == "") {
                $("#description").focus();
                Alert('Please enter  description', '', 'error');
                return;
            }
            if (icon == "") {
                $("#photo_id_input").focus();
                Alert('Please select  icon', '', 'error');
                return;
            }
            if (filename == "") {
                $("#filename").focus();
                Alert('Please enter  filename', '', 'error');
                return;
            }

            createContent(name, description, icon, filename);

        });

        function createContent(name, description, icon, filename) {
            $("#loader").show();

            var data = new FormData();
            var photo_id = $('#photo_id_input').prop('files')[0];
            data.append('name', name);
            data.append('description', description);
            data.append('icon', photo_id);
            data.append('filename', filename);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_content_create') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalCreateContent").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });

        }
    </script>
@endsection
<div class="modal fade" id="modalCreateContent">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <input type="hidden" id="user_id">
                <h4 class="modal-title" id="contentTitle">Create new content</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name" class="form-control-label">Content Name:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-folder-o"></i></span>
                        <input id="name" type="text" class="form-control" placeholder="Enter content name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="description" class="form-control-label">Description:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="description" type="text" class="form-control" placeholder="Enter an description">
                    </div>
                </div>
                <div class="form-group">
                    <label for="icon" class="form-control-label">Icon:</label>
                    {{--<div class="input-group">--}}
                    {{--<span class="input-group-addon"><i class="fa fa-pencil"></i></span>--}}
                    {{--<textarea id="icon" type="text" class="form-control" placeholder="Select icon" ></textarea>--}}
                    <p class="text-center">
                        <img src="" width="100px" height="100px" id="photo_id_update">
                    </p>
                    <p class="">
                        <label for="photo_id_input" style="padding-top: 10px;cursor: hand">
                            <a>
                                <span class="glyphicon glyphicon-folder-open text-dapp" aria-hidden="true"></span>&nbsp;
                                Choose an image
                            </a>
                            <input type="file" id="photo_id_input" class="text-center" accept="image/*"
                                   style="display:none" onchange="loadFile(event)">
                        </label>
                    </p>
                </div>
                <div class="form-group">
                    <label for="filename" class="form-control-label">Filename:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                        <textarea id="filename" type="text" class="form-control"
                                  placeholder="Enter filename"></textarea>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="btnCreateContent" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    var loadFile = function (event) {
        var fileSize = $('#photo_id_input').prop('files')[0].size;
        var fileType = $('#photo_id_input').prop('files')[0].type;
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) > 0) {
            if (fileSize > 2097152) {
                swal("Warning", 'Image size too large, accept image with under 2 Mb only', 'warning');
                $('#photo_id_input').replaceWith(input.val('').clone(true));
                return false;
            }
            else {
                var photo_id = document.getElementById('photo_id_update');
                photo_id.src = URL.createObjectURL(event.target.files[0]);
                return true;
            }
        }
        else {
            $('#photo_id_input').replaceWith(input.val('').clone(true));
            swal("Oops!", 'Please choose an image with *.png, *.jpg format', 'warning');
            return false;
        }
    }

</script>
<div class="modal fade" id="modalCreateQuestion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <input type="hidden" id="user_id">
                <h4 class="modal-title" id="contentTitle">Create question</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="question" class="form-control-label">Question:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="question" type="text" class="form-control" placeholder="Enter an question">
                    </div>
                </div>
                <div class="form-group">
                    <label for="question_vi" class="form-control-label">Vietnamese Question:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="question_vi" type="text" class="form-control" placeholder="Enter an Vietnamese question">
                    </div>
                </div>
                <div class="form-group">
                    <label for="mp3" class="form-control-label">Mp3(.mp3):</label>
                        <input type="file" name="file" id="mp3Input" accept=".mp3"/>
                        <input type="file" id="mp3Input" class="text-center" accept=".mp3"
                               style="display:none" onchange="loadMP3(event)">
                        </label>
                </div>
                <div class="form-group">
                    <label for="lesson_id" class="form-control-label">Lesson name:</label>
                    <select class="form-control input-sm" name="content_name" id="lesson_id">
                        @foreach($lessons as $key=>$lesson)
                            <option value="{{$lesson->id}}">{{ $lesson->name}}     </option>
                        @endforeach
                    </select>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="btnCreateQuestion" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<script>

</script>
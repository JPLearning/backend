@section('styles')
    <style>
        .middle {
            vertical-align: middle !important;
        }

        .box {
            overflow-x: scroll;
        }
    </style>
@endsection
@extends('admin.common.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div id="loader" class="loading" hidden>Loading&#8230;</div>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Users
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3 id="user_count">123</h3>
                            <p>Totals</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-ios-people"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3 id="free_account">1212</h3>
                            <p>Free account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3 id="pro_account">1212</h3>
                            <p>Pro account</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-cash"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>65</h3>
                            <p>Inactive</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-thumbsdown"></i>
                        </div>
                        <a class="small-box-footer"></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="box">
                <div class="box-header">
                    <a class="btn btn-primary" id="createAnswer"><i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="tbl_answers" class="table table-bordered table-striped table-responsive">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Answer</th>
                            <th class="text-center">Quesion</th>
                            <th class="text-center">Yes</th>
                            <th class="text-center">Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($answers as $key=>$answer)
                            <tr id="{!! $answer['id'] !!}">
                                <td class="text-center middle">{!! $key+1 !!}</td>
                                <td class="middle">{!! $answer->answer !!}</td>
                                <td class="middle">{!! $answer->Question->question !!}</td>
                                <td class="middle">{!! $answer->yes !!}</td>
                                <td class="text-center middle">
                                    <a class="btnEdit btn btn-info btn-sm">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <a class="btnDelete btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o fa-lg"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.answer.create')
    <!--edit leson-->
    <div class="modal fade" id="modalEdit" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <input type="hidden" id="sentence_id_edit">
                    <h4 class="modal-title" id="user_name"></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="sentence_jp_InputEdit" class="form-control-label">Sentence Japanese:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="sentence_jp_InputEdit" type="text" class="form-control" placeholder="Enter sentence Japanese">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="sentence_vi_InputEdit" class="form-control-label">Sentence Vietnamese:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="sentence_vi_InputEdit" type="text" class="form-control" placeholder="Enter sentence Vietnamese">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mp3InputEdit" class="form-control-label">File mp3(.mp3):</label>
                        <input type="file" name="file" id="mp3InputEdit" accept=".mp3"/>
                    </div>
                    <div class="form-group">
                        <label for="type_InputEdit" class="form-control-label">Type:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="type_InputEdit" type="text" class="form-control" placeholder="Enter type">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="learn_InputEdit" class="form-control-label">Learn:</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input id="learn_InputEdit" type="text" class="form-control" placeholder="Enter learn">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="lesson_idEdit" class="form-control-label">Lesson name:</label>
                        <select class="form-control input-sm" name="content_name" id="lesson_idEdit">
                            @foreach($questions as $key=>$question)
                                <option value="{{$question->id}}">{{ $question->question}}     </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button id="btnSave" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('/public/libs/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/public/libs/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('#tbl_questions').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        });

        function Alert(title, message, type) {
            swal({
                title: title,
                text: message,
                type: type,
                showConfirmButton: true
            });
        }

        $("#createAnswer").click(function () {
            $("#modalCreateAnswer").modal();
        });

        $(".btnDelete").click(function () {
            var id = $(this).closest('tr').attr('id');
            swal({
                    title: "Are you sure?",
                    text: "Do you really want to delete this lesson!",
                    type: "error",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    showLoaderOnConfirm: true,
                    closeOnConfirm: false
                },
                function () {
                    setTimeout(function () {
                        deleteLeson(id);
                    }, 1000);
                });
        });

        $(".btnEdit").click(function () {
            var id = $(this).closest('tr').attr('id');
            getLesson(id);
            $("#modalEdit").modal();
        });

        $("#btnSave").click(function () {
            var id = $("#sentence_id_edit").val();
            var sentence_jp = $("#sentence_jp_InputEdit").val();
            var sentence_vi = $("#sentence_vi_InputEdit").val();
            var mp3 = $("#mp3InputEdit").val();
            var type = $("#type_InputEdit").val();
            var learn = $("#learn_InputEdit").val();
            var lesson_id = $("#lesson_idEdit").val();
            if (sentence_jp == "") {
                $("#sentence_jp_InputEdit").focus();
                Alert('Please enter sentence Japanese', '', 'error');
                return;
            }
            if (sentence_vi == "") {
                $("#sentence_vi_InputEdit").focus();
                Alert('Please enter sentence Vietnamese', '', 'error');
                return;
            }
            if (mp3 == "") {
                $("#mp3InputEdit").focus();
                Alert('Please select file mp3', '', 'error');
                return;
            }
            if (type == "") {
                $("#type_InputEdit").focus();
                Alert('Please enter type', '', 'error');
                return;
            }
            if (lesson_id == "") {
                $("#lesson_idEdit").focus();
                Alert('Please select lesson', '', 'error');
                return;
            }
            updateSentence(id, sentence_jp, sentence_vi, mp3, type,learn, lesson_id);
        });

        function getLesson(id) {
            $.ajax({
                data: {id: id, _token: '{!! csrf_token() !!}'},
                url: '{{ route('admin_sentence_get') }}',
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    console.log(data)
                    $("#sentence_id_edit").val(data.sentence.id);
                    $("#sentence_jp_InputEdit").val(data.sentence.sentence_jp);
                    $("#sentence_vi_InputEdit").val(data.sentence.sentence_vi);
                    $("#mp3InputEdit").attr('src', '/resources/upload/lesson'+data.sentence.mp3);
                   // $("#mp3InputEdit").val(data.sentence.mp3);
                    $("#type_InputEdit").val(data.sentence.type);
                    $("#learn_InputEdit").val(data.sentence.learn);
                    $("#lesson_idEdit").val(data.sentence.lesson_id);
                    $("#modalEdit").modal();
                }
            });
        }
        function updateSentence(id, sentence_jp, sentence_vi, mp3, type, learn,lesson_id) {
            $("#loader").show();
            var data = new FormData();
            var mp3_id = $('#mp3InputEdit').prop('files')[0];
            data.append('id', id)
            data.append('sentence_jp', sentence_jp);
            data.append('sentence_vi', sentence_vi);
            data.append('mp3', mp3_id);
            data.append('type', type);
            data.append('learn', learn);
            data.append('lesson_id', lesson_id);
            console.log(data);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });
            $.ajax({
                url: '{!! route('admin_sentence_update') !!}',
                type: 'POST',
                dataType: 'json',
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    console.log(data)
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#modalEdit").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    console.log(data)
                    Alert(data.status, data.message, data.type);
                    $("#loader").hide();
                }
            });
        }

        function deleteLeson(id) {
            $.ajax({
                type: 'POST',
                url: '{!! route('admin_lesson_delete') !!}',
                data: {id: id, _token: '{{ csrf_token() }}'},
                success: function (data) {
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                        $("#" + id).remove();
                    }
                    else {
                        Alert(data.status, data.message, data.type);

                    }
                },
                error: function (data) {
                    Alert('Something went wrong', '', 'error');
                }
            });
        }


        $("#btnCreateAnswer").click(function () {

                var answer = $("#answer").val();
                var question_id = $("#question_id").val();
                var yes = $("#yes").val();

                if (answer == "") {
                    $("#answer").focus();
                    Alert('Please enter answer', '', 'error');
                    return;
                }

                if (question_id == "") {
                    $("#question_id").focus();
                    Alert('Select question', '', 'error');
                    return;
                }
            if (yes == "") {
                $("#yes").focus();
                Alert('Select check', '', 'error');
                return;
            }

            createAnswer( answer,question_id,yes);

            }
        );

        function createAnswer(answer,question_id,yes) {

            $("#loader").show();
            var data = {
                answer: answer,
                question_id: question_id,
                yes: yes,
                _token: '{{ csrf_token() }}'
            }
            $.ajax({
                type: 'POST',
                url: '{!! route('admin_answer_create') !!}',
                data: data,
                success: function (data) {
                    if (data.status == 'success') {
                        Alert(data.status, data.message, data.type);
                      //  $("#modalCreateContent").modal('hide');
                        $("#loader").hide();
                    }
                    else {
                        Alert(data.status, data.message, data.type);
                        $("#loader").hide();

                    }
                },
                error: function (data) {
                    Alert('Something went wrong', '', 'error');
                    $("#loader").hide();
                }
            });

        }


    </script>
@endsection
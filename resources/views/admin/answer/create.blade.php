<div class="modal fade" id="modalCreateAnswer">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <input type="hidden" id="user_id">
                <h4 class="modal-title" id="contentTitle">Create answer</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="answer" class="form-control-label">Answer:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="answer" type="text" class="form-control" placeholder="Enter an answer">
                    </div>
                </div>
                <div class="form-group">
                    <label for="question_id" class="form-control-label">Question:</label>
                    <select class="form-control input-sm" name="content_name" id="question_id">
                        @foreach($questions as $key=>$question)
                            <option value="{{$question->id}}">{{ $question->question}}     </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="yes" class="form-control-label">Yes:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input id="yes" type="text" class="form-control" placeholder="Select right answer">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button id="btnCreateAnswer" type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<script>

</script>